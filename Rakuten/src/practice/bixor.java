package practice;

public class bixor {
	
	public static void main(String[] args) {
		int x = getRangedXOR(2,12);
		System.out.println(x);
	}

	private static int getRangedXOR(int i, int j) {
		return f(j)^f(i-1);
	}

	private static int f(int i) {
		int res[] = {i, 1, i+1,0};
		return res[i%4];
	}

}
