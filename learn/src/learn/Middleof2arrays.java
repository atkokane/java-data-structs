package learn;

public class Middleof2arrays {



	public static void main(String[] args) {
		
		int[] a1 = new int[]{1,2,3,4,5};
		int[] a2 = new int[]{5,4,3,2,1};
		int res[] = new int[2];
		res[0] = a1[a1.length/2];
		res[1] = a2[a2.length/2];
		//res = new int[]{a1[a1.length/2], a2[a2.length/2]};
		
		System.out.println(res[0]+" "+res[1]);
	}
}
