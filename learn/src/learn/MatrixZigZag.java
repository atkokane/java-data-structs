package learn;

public class MatrixZigZag {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char[][] mat = new char[][] {
			{'A','B','C','D'},
			{'E','F','G','H'},
			{'I','J','K','L'}};
		int n = 3, m=4;
		String tempString = "";
		for (int line = 1; line <= (m + n - 1); line++) {
		    int start_col = Math.max(0, line - n);
		    int count = Math.min(Math.min(line, (m - start_col)), n);
		    tempString = "";
		    for (int j = 0; j < count; j++) {
		        tempString = tempString + mat[Math.max(n, line) - j - 1][start_col + j];
		    }
		    System.out.println(tempString);
		}
	}
}
