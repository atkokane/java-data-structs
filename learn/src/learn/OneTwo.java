 package learn;import javax.jws.Oneway;

/*
 * Given a string, compute a new string by moving the first char to come after the next two chars,
 *  so "abc" yields "bca". Repeat this process for each subsequent group of 3 chars, 
 *  so "abcdef" yields "bcaefd". Ignore any group of fewer than 3 chars at the end.
 *  
 *  oneTwo("abc") → "bca"
 *  oneTwo("tca") → "cat"
*/
public class OneTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(starOut("*aasdas*"));

	}
	public static String oneTwo(String str) {
		int len = str.length();
		String strin="";
		if(len%3 != 0) {
			
			strin = str.substring(0,len-(len%3));
		}
		char[] strArr = strin.toCharArray();
		System.out.println(strArr+""+(len-3));
		if(str.length() <3) return "";
		int i;
		for(i = 0;i <= len-3; i++) {
			if(i%3 == 0){
				char temp = strArr[i];
				strArr[i] = strArr[i+1];
				strArr[i+1] = strArr[i+2];			
				strArr[i+2] = temp;
			}
		}
		
		return new String(strArr).substring(0, i+1);
		
	}

	
	/*Return a version of the given string, where for every star (*) in the string the star and the chars immediately to its left and right are gone. 
	 * So "ab*cd" yields "ad" and "ab**cd" also yields "ad".
	 * starOut("ab*cd") → "ad"
	 * starOut("ab**cd") → "ad"
	 * starOut("sm*eilly") → "silly"*/
	
	public static String starOut(String str) {
		if(str.equals("") || str.equals("*")) return "";
		if(str.startsWith("*")) str = str.substring(2);
		if(str.length()-1 == str.lastIndexOf("*")) str = str.substring(0, str.lastIndexOf("*")-1);
		
		
		System.out.println(str);
		
		return "";
		
	}
}
