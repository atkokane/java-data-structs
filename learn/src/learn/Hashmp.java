package learn;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Hashmp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        Map phoneNo = new HashMap<String, Integer>();
	        for(int i = 0; i < n; i++){
	            String name = in.next();
	            int phone = in.nextInt();
	            phoneNo.put(name, new Integer(phone));
	            // Write code here
	        }
	        while(in.hasNext()){
	            String s = in.next();
	            // Write code here
	            if (phoneNo.containsKey(s)) {
	            	System.out.println(s+"="+phoneNo.get(s));
	            } else {
	            	System.out.println("Not found");
	            }
	        }
	        in.close();
	}

}
