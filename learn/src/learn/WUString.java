package learn;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class WUString {

    /*
     * Complete the weightedUniformStrings function below.
     */
    static String[] weightedUniformStrings(String s, int[] queries) {
        /*
         * Write your code here.
         */
        char[] sArr = new char[s.length()]; 
        String [] res = new String[queries.length];
        for(int i=0;i<queries.length;i++) {
        	res[i] = "No";
        }
        sArr = s.toCharArray();
        for(int i = 0;i<s.length();i++) {
            int count = getCount(sArr,i);
            //System.err.println(count);
            if (count>1)
            	i = i+count-1;
            int weight = count * ((int)sArr[i]-96);
            int pos = getPos(queries, ((int)sArr[i]-96));
            if (pos != -1)
            	res[pos] = "Yes";
            pos = getPos(queries, weight);
            if (pos != -1)
            	res[pos] = "Yes";
        }
        return res;
    }
    
    static int getCount(char[] arr, int i) {
        int counter =0;
        char c = arr[i];
        //System.out.println(""+arr[i]);
        for (int x = i;x<arr.length;x++) 
        if (c == arr[x]) 
           counter++;// = counter+getCount(arr,i+1);
        else break;
        //System.out.println(c+" " +counter);
        return counter;
    }

        static int getPos(int[] arr, int wt) {
            int pos = -1;
            for(int q =0; q < arr.length;q++) {
                if (arr[q] == wt) pos = q;
            }
            return pos;
        }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = "abccddde";//scanner.nextLine();

        int queriesCount = 6;//Integer.parseInt(scanner.nextLine().trim());

        int[] queries = new int[]{1,3, 12,5,9,10};
        

/*        for (int queriesItr = 0; queriesItr < queriesCount; queriesItr++) {
            int queriesItem = Integer.parseInt(scanner.nextLine().trim());
            queries[queriesItr] = queriesItem;
        }*/

        String[] result = weightedUniformStrings(s, queries);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            /*bufferedWriter.write(result[resultItr]);

            if (resultItr != result.length - 1) {
                bufferedWriter.write("\n");
            }*/
        	System.out.println(result[resultItr]);
        }

       /* bufferedWriter.newLine();

        bufferedWriter.close();*/
    }
}
