package learn;

	import java.io.*;
	import java.util.*;
	import java.text.*;
	import java.math.*;
	import java.util.regex.*;

	public class CeaserCypher {

	    static String caesarCipher(String s, int k) {
	        // Complete this function
	        
	        char[] arr = new char[s.length()];
	        arr = s.toCharArray();
	        for (int i=0;i<s.length();i++) {
	            int c = (int) arr[i];
	            if (k>26) k = k%26;
	            if (c>=97 && c<=122) {
	            	c = (char)(c+k);
	            	if(c>122) c = ((int)'a')+ c%122 - 1;
	            } else if (c>=65 && c<=90){
	            	c = (char)(c+k);
	            	if (c>90) c = ((int)'A')+(c+k)%90 -1;
	            }
	           // System.out.println(c);
	            arr[i] = (char)c;
	        }
	        return new String(arr);
	    }

	    public static void main(String[] args) {
	        //Scanner in = new Scanner(System.in);
	        //int n = in.nextInt();
	        String s = "!m-rB`-oN!.W`cLAcVbN";//in.next();
	        //int k = in.nextInt();
	        String result = caesarCipher(s, 62);
	        System.out.println(result);
	        //in.close();
	    }
	}
