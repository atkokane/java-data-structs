package learn;

//public class Cafeteria {

public class Cafeteria {
	public static void main(String[] args) {
		Outer out = new Outer() {
			 Outer.Inner2 inner2 = new Outer.Inner2() {
				public void method1() {
					System.out.println(Outer.x);
					System.out.println("m1");
				}
			};
		};
		//out.inner2.method1();
	}
}

class Outer {
	static int x = 5;

	static class Inner2 extends Outer {
		public int x = 20;

		public void method1() {
			System.out.println("m3");
		}

		public void method2() {
			System.out.println("m4");
		}
	}
}

/*
 * } public static void main(String[] args) { Outer.Inner1 inner1 = new
 * Outer().new Inner2(); inner1.method1(); } }
 * 
 * class Outer { int x = 5;
 * 
 * static { System.out.println("Outer"); }
 * 
 * static class Inner1 { static { System.out.println("Inner_static1 "); }
 * 
 * public static int x = 10;
 * 
 * public void method1() { new Outer().new Inner2().method1();
 * System.out.println(this.x); }
 * 
 * public void method2() { System.out.println("m2"); } }
 * 
 * class Inner2 extends Inner1 { public int x = 20;
 * 
 * public void method1() { System.out.println(Inner1.this.x); }
 * 
 * public void method2() { System.out.println("m4"); } } }
 */