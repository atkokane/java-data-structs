package hackerRank;

public class ChaoticYr {

	static void minimumBribes(int[] q) {
		int counter = 0;
		int diff = 0;
		for (int i=0;i<q.length-1;i++) {
			if (q[i]>q[i+1]) {
				diff = q[i]-q[i+1];
				if (diff<=2) 
					counter += diff;
				else {
					System.out.println("Too chaotic");
					return;
				}
			}
		}
		System.out.println(counter);
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int q[] = {2,5,1,3,4}; 
		minimumBribes(q);
	}

}
