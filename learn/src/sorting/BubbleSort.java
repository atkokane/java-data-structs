package sorting;

public class BubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = new int[] {40, 2, 1, 43, 3, 65, 0, -1, 58, 3, 42, 4};
		bubbleSort(arr);
		
	}

	private static void bubbleSort(int[] arr) {
		// TODO Auto-generated method stub
		for(int i = 0;i<arr.length; i++) {
			for (int j = 0;j < arr.length-i-1; j++) {
				if (arr[j] > arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
				if (i==0) {
					for (int k : arr) {
						System.out.print(" "+k);
					}
					System.out.println();

				}
			}
		}
		System.out.println();
		for (int i : arr) {
			System.out.print(" "+i);
		}
	}

}
