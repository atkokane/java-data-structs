package sorting;

public class Insertion {
	static int[] arr = new int[]{5,8,1,3,9,6};
	public static void main(String[] args) {
		insertionSort();
	}
	private static void insertionSort() {
		// TODO Auto-generated method stub
		for (int i=1;i<arr.length;i++) {
			System.out.println();
			int j = i-1;
			int key = arr[i];
			while(j >= 0 && arr[j] > key) {
				int temp = arr[j+1];
				arr[j+1] = arr[j];
				arr[j] = temp;
				j--;
			}
		}
	}
}
