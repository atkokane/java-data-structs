package sorting;

public class BidirectionalBubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		bidirectionalBubbling();
	}

	private static void bidirectionalBubbling() {
		// TODO Auto-generated method stub
		int[] arr = new int[] {40, 2, 1, 43, 3, 65, 0, -1, 58, 3, 42, 4};
		int mid = arr.length/2;
		for (int i = 0; i< arr.length; i++) {
			
			for (int j = 0; j< arr.length-i-1; j++) {
				if (j >= mid) continue;
				else if (arr[j]> arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
			
			for (int k = arr.length-1; k >= i+1 ;k--) {
				if (k <= mid) continue;
				else if (arr[k]<arr[k-1]) {
					int temp = arr[k];
					arr[k] = arr[k-1];
					arr[k-1] = temp;
				}
			}
			
			for (int f : arr) {
				System.out.print(" "+f);
			}
			System.out.println();
		}
		
		for (int i : arr) {
			System.out.print(" "+i);
		}
	}

}
