package sorting;

public class SelectionSort {

	public static void main(String[] args) {
		selectionSort();
		
	}

	private static void selectionSort() {
		// TODO Auto-generated method stub
		int[] arr = new int[]  {40, 2, 1, 43, 3, 65, 0, -1, 58, 3, 42, 4};
		
		int unsorted = arr.length-1;
		while (unsorted >= 0) {
			int maxIndex = 0;
			for (int i=0; i<=unsorted;i++) {	
				if (arr[i]> arr[maxIndex]) 
					maxIndex = i;
			}
			int temp = arr[unsorted];
			arr[unsorted] = arr[maxIndex];
			arr[maxIndex] = temp;
			unsorted--;
			for (int i : arr) {
				System.out.print(" "+i);
			}
			System.out.println();
		}
		
		
		
	}
}
