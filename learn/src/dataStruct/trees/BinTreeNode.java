package dataStruct.trees;

public class BinTreeNode {

	BinTreeNode left, right;
	int data;
	public BinTreeNode getLeft() {
		return left;
	}
	public void setLeft(BinTreeNode left) {
		this.left = left;
	}
	public BinTreeNode getRight() {
		return right;
	}
	public void setRight(BinTreeNode right) {
		this.right = right;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	
	public BinTreeNode(int key) {
		data = key;
		left = null;
		right = null;
	}
}
