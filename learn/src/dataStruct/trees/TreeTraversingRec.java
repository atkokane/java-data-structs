package dataStruct.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class TreeTraversingRec {

	public static void main(String[] args) {

		BinTreeNode node = new BinTreeNode(4);
		node.setLeft(new BinTreeNode(2));
		node.setRight(new BinTreeNode(5));
		node.left.setLeft(new BinTreeNode(1));
		node.left.setRight(new BinTreeNode(3));		
		//node.right.setLeft(new BinTreeNode(7));
		node.right.setRight(new BinTreeNode(6));		
		System.out.println("-----------InOrder-------------");
		//inOrderTraverse(node);
		int arr[] = getInOrderTree(node);
		for (int string : arr) {
			System.out.print(" "+string);
		}
		System.out.println();
		System.out.println("-----------PreOrder-------------");		
		preOrderTraverse(node);
		System.out.println("\n-----------PostOrder-------------");
		postOrderTraverse(node);
		System.out.println("\n-----------LeftOrder-------------");
		leftView(node);
		System.out.println("\n-----------BFS-------------");
		bfs(node);
		System.out.println("\n-----------heighest of tree-------------");
		System.out.println(getHeighestOfTree(node));
	/*	System.out.println("\n-----------insert into a tree-------------");*/
		insertIntoTree(node, 10);
		System.out.println("\n-----------Size of tree-------------");
		System.out.println(getSizeOfTree(node));
		System.out.println("\n-----------IS BST-------------"+isBST(node));
		
		

	}
	
	private static boolean isBST(BinTreeNode node) {
		int[] arr = getInOrderTree(node);
		return isSorted(arr);
	}

	private static boolean isSorted(int[] arr) {
		for (int i = 0;i<arr.length-1;i++)
			if (arr[i]>arr[i+1])
				return false;
		return true;
	}

	private static int[] getInOrderTree(BinTreeNode node) {
		List<Integer> inorderList = new ArrayList<Integer>();
		if (node == null) return null;
		else {
			Stack<BinTreeNode> s = new Stack<BinTreeNode>();
			BinTreeNode current = node;
			while(!s.isEmpty() || current != null) {
				if (current != null) {
					s.push(current);
					current = current.getLeft();
				} else {
					BinTreeNode t = s.pop();
					inorderList.add(t.getData());
					current = t.getRight();
				}
			}
			
			int[] arr = new int[inorderList.size()];
			for (int i = 0;i<inorderList.size();i++) {
				arr[i] = inorderList.get(i);
			}
			return arr;
			
		}
	}

	private static int getSizeOfTree(BinTreeNode node) {
		if (node == null)
			return 0;
		else {
			int size = 1;
			Queue<BinTreeNode> q = new LinkedList<>();
			q.add(node);
			while(!q.isEmpty()) {
				BinTreeNode n = q.poll();
				if (n.getLeft() != null) {
					size++;
					q.add(n.getLeft());
				}
				if (n.getRight() != null) {
					size ++;
					q.add(n.getRight());
				}
			}
			return size;
		}
	}

	private static void insertIntoTree(BinTreeNode node, int i) {
		if (node == null)
			System.out.println("Node will be created and will be root");
		else {
			if (node.getLeft() == null) 
				node.setLeft(new BinTreeNode(i));
			else if (node.getRight() == null)
				node.setRight(new BinTreeNode(i));
			else insertIntoTree(node.getLeft(), i);
		}
	}

	static int heighest = -1;

	private static int getHeighestOfTree(BinTreeNode node) {
		if (node != null) {
			Queue<BinTreeNode> q = new LinkedList<>();
			q.add(node);
			while(!q.isEmpty()) {
				BinTreeNode n = q.poll();
				if(heighest <n.getData()) heighest = n.getData();
				if (n.getLeft()!= null) 
					q.add(n.getLeft());
				if (n.getRight()!= null) 
					q.add(n.getRight());
			}
		}
		return heighest;
	}

	private static void bfs(BinTreeNode node) {
		Queue<BinTreeNode> list = new LinkedList<BinTreeNode>();
		list.add(node);
		while (list.isEmpty() != true) {
			BinTreeNode n = (BinTreeNode) list.poll();
			System.out.print(n.getData()+" ");
			if (n.getLeft() != null)
				list.add(n.getLeft());
			if (n.getRight() != null)
				list.add(n.getRight());
				
		}
	}

	private static void leftView(BinTreeNode node) {
		// TODO Auto-generated method stub
		if (node != null) {
			System.out.print(node.getData()+" ");
			while (node.getLeft() != null) {
				node = node.getLeft();
				System.out.print(node.getData()+" ");
			}
		}
	}

	private static void postOrderTraverse(BinTreeNode node) {
		if (node != null) {
			postOrderTraverse(node.getLeft());
			postOrderTraverse(node.getRight());
			System.out.print(node.getData()+" ");
		}
	}

	private static void preOrderTraverse(BinTreeNode node) {
		if (node != null) {
			System.out.print(node.getData()+" ");
			preOrderTraverse(node.getLeft());
			preOrderTraverse(node.getRight());
		}
	}

	private static void inOrderTraverse(BinTreeNode node) {
		if (node != null) {
			inOrderTraverse(node.getLeft());
			System.out.print(node.getData()+" ");
			inOrderTraverse(node.getRight());
		}
	}

}
