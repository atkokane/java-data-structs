package dataStruct;

class ListNode {

	int Data;
	public int getData() {
		return Data;
	}

	public void setData(int data) {
		Data = data;
	}

	public ListNode getNextNode() {
		return nextNode;
	}

	public void setNextNode(ListNode nextNode) {
		this.nextNode = nextNode;
	}

	ListNode nextNode;
	
	public ListNode(int data) {
		Data = data;
	}	
}
