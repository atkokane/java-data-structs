package dataStruct;

public class LinkedListOperations {

	ListNode head, temp;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListOperations LLOps = new LinkedListOperations();
		LLOps.insertAtBeginning(12);
		LLOps.insertAtBeginning(23);
		LLOps.insertAtBeginning(41);
		LLOps.insertAtBeginning(27);
		LLOps.insertAtPosition(64, 3);
		
		System.out.println("Linked List data");
		LLOps.traverseList();
		//LLOps.getNthNodeData(2);
		//LLOps.deleteFirstNode();
		//LLOps.deleteAtPosition(5);
		LLOps.deleteLastNode();
		LLOps.insertAtLast(15);
		System.out.println("\nLinked list after deleting first node");
		LLOps.traverseList();
	}
	
	boolean insertAtBeginning(int data) {
		ListNode node = new ListNode(data);
		boolean sucess = false;
		if (isLLEmpty()) {
			head = node;
			sucess = true;
		} else {
			//temp = head;
			node.setNextNode(head);
			head  = node;
			sucess = true;
		}
		return sucess;
	}
	
	void traverseList() {
		if (isLLEmpty()) System.out.println("list is empty");
		else {
			temp = head;
			while(temp != null) {
				System.out.print(temp.getData()+" ");
				temp = temp.getNextNode();
			}
		}
	}

	boolean isLLEmpty() {
		return (head == null);			
	}
	
	boolean hasNext(ListNode head) {
		if (head == null) return false;
		else return (head.getNextNode() != null);
	}
	
	void deleteFirstNode(){
		if (isLLEmpty()) System.out.println("List is empty");
		else {
			temp = head;
			head = head.getNextNode();
			temp = null;
		}
	}
	
	void insertAtPosition(int data, int pos) {
		if (isLLEmpty()||pos == 1) insertAtBeginning(data);
		else {
			temp = head;
			int counter = 0;
			ListNode newNode = new ListNode(data);
			while ((temp.getNextNode()) != null) {
				counter++;
				if (pos-1 == counter) {
					//System.out.println(pos + " position node data is "+temp.getData());
					newNode.setNextNode(temp.getNextNode());
					temp.setNextNode(newNode);
					break;
				}
				temp = temp.getNextNode();			}
		}
	}
	
	int getNthNodeData(int pos) {
		int counter = 0;
		temp = head;
		if (pos == 0 || isLLEmpty()) System.out.println("list is empty...");
		while (temp.getNextNode() != null) {
			counter++;
			if (pos == counter) {
				System.out.println(pos + " position node data is "+temp.getData());
				break;
			}
			temp = temp.getNextNode();
		}
		return -1;
	}

	void deleteAtPosition(int pos) {
		if (isLLEmpty()) System.out.println("list is empty...");
		else {
			temp = head;
			int counter = 0;
			while ((temp.getNextNode()) != null) {
				counter++;
				if (pos-1 == counter) {
					//System.out.println(pos + " position node data is "+temp.getData());
					ListNode deleteNode = temp.getNextNode();
					temp.setNextNode(deleteNode.getNextNode());
					break;
				}
				temp = temp.getNextNode();	
			}
		}
	}

	void deleteLastNode() {
		temp = head;
		while(temp.nextNode.nextNode != null){
			temp = temp.getNextNode();
		}
		temp.setNextNode(null);
	}
	
	 void insertAtLast(int data) {
			temp = head;
			while((temp.getNextNode()) != null){
				 temp = temp.getNextNode();
			}
			temp.setNextNode(new ListNode(data));
		 
	 }
}
