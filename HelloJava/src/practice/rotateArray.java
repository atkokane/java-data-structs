package practice;

public class rotateArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		rotate(new int[] {-1,-100,3,99}, 2);
		
	}

	public static void rotate(int[] nums, int k) {
	
		if (nums.length<1) return;

		while(k>0) {
			rotateBy1(nums);
			k--;
		}		
		System.out.println();
		for (int i : nums) {
			System.out.print(i);
		}
    }

	private static void rotateBy1(int[] nums) {
		// TODO Auto-generated method stub
		int temp = nums[nums.length-1];

		for(int i=nums.length-1;i>=1;i--) {
			nums[i] = nums[i-1];
		}
		nums[0] = temp;
		for (int i : nums) {
			System.out.print(i);
		}
		System.out.println();
	}
	
	
}
