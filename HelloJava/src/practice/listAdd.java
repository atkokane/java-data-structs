package practice;

import java.awt.List;

public class listAdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ListNode n1 = new ListNode(2);
		n1.next = new ListNode(4);
		n1.next.next = new ListNode(3);
		
		ListNode n2 = new ListNode(5);
		n2.next = new ListNode(6);
		n2.next.next = new ListNode(4);
		
		addTwoNumbers(n1, n2);
	}

	 public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		 ListNode n;
		 if (l1 == null || l2 == null) return null;
		 int v1 = 0;
		 int v2 = 0;
		 int counter = 0;
		 while(l1 != null && l2 != null) {
			 int tens = (int)Math.pow(10, counter++);
			 v1 = v1  + l1.val * tens;
			 v2 = v2 + l2.val * tens;
			 //System.out.println(v1);
			 l1 = l1.next;
			 l2 = l2.next;
		 }
		 System.out.println(v1+v2);
		 int x = v1+v2;
		 int data = x%10;
		 n = new ListNode(data);
		 ListNode curr = n;
		 x = x/10;
		 while(x > 0) {
			 
			 data = x%10;
			 x = x/10;
			 n.next = new ListNode(data);
			 n = n.next;
		 }

		 while (curr != null) {
			 System.out.print(curr.val+"->");
			 curr = curr.next;
		 }
		 return n;
	    }
}
