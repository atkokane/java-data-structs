package practice;

public class Dec2Bin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int no = 7;
		getBinRep(no);
	}

	private static int[] getBinRep(int no) {
		// TODO Auto-generated method stub
		int[] arr = new int[8];
		int i = 7;
		while (no>0) {
			arr[i--] = no%2;
			no = no/2;
		}
		for (int n : arr) {
			System.out.print(n);
		}
		System.out.println();
		return arr;
	}

}
